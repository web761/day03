<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <div id="main">

        <div class="wrapper">
            <form action="" method="post">
                <?php echo '<div class="form-group">
                    <label class="form-label">Họ và tên</label>
                    <input class="name" type="text" name="username" id="">    
                </div>' ?>

                <div class="form-group">
                <label class="form-label">Giới tính</label>
                <?php
                $gender = array("0" => "Nam", "1" => "Nữ");
                for ($i = 0; $i < count($gender); $i++) {
                    echo '  <input type="radio"  name="gender" value=" ' . $i . '">';
                    echo '<label class="labelRadio" for="html">' . $gender[$i] . '</label>';
                }
                ?>
                </div>
                <div class="form-group">
                    <label class="form-label">Phân khoa</label>
                    <select name="falcuty" id="falcuties">
                        <?php
                            $falcuty = array("SPA" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học dữ liệu");
                            foreach ($falcuty as $key => $value) {
                                echo '<option value="' . $key . '">' . $value . '</option>';
                            }
                        ?>
                    </select>
                </div>

                <button class="form-button">Đăng ký</button>
            </form>
        </div>
    </div>
</body>

</html>
<!--   -->